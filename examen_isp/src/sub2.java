//Implement a Java GUI application (with Swing and AWT)  composed of one text fields and one button. When clicking the button the text input in the text field is written backwards  in the console.
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.MappedByteBuffer;

class Application2 implements ActionListener {

    String string;
    JFrame panel = new JFrame();
    JTextField textField1 = new JTextField();
    JButton button1 = new JButton();

    public Application2() {

        textField1.setBounds(50, 50, 300, 40);

        button1.setBounds(150, 200, 100, 25);
        button1.addActionListener(this);

        panel.add(textField1);
        panel.add(button1);
        panel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel.setSize(400, 400);
        panel.setLayout(null);
        panel.setVisible(true);
    }

    public static void main(String[] args) {
        Application2 app = new Application2();
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button1) {
            string = textField1.getText();
            StringBuilder output = new StringBuilder();
            output.append(string);
            output.reverse();
            System.out.println(output);
            /*char[] backwards = string.toCharArray();
            System.out.println(string);
            int i;
            for(i=0;i<backwards.length;i++){
                System.out.println(backwards[i]);
            }*/
        }
    }
}