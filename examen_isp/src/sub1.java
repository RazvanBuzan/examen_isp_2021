public class sub1 {
    class K {
    }

    class L extends K {
        private int a;
        private M m;

        public void b() {
        }

        public void i() {
        }
    }

    class M {
    }

    class B {
        public M m;

        B() {
            this.m = new M();
        }
    }

    class A {
        public M m;
    }

    class X {
        public void met(L l) {
        }
    }
}
